<?php

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CouponController;
use App\Http\Controllers\ColorController;
use App\Http\Controllers\SizeController;
use App\Http\Controllers\ProductController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

// Route::middleware(['admin.auth'])->group(function () {
  Route::get('/', [AdminController::class ,'index']);
    Route::prefix('admin')->group(function () {
    Route::get('/', [AdminController::class ,'index']);
    Route::post('/auth', [AdminController::class ,'auth'])->name('admin.auth');
Route::middleware(['admin.auth'])->group(function () {
    Route::get('/dashboared', [AdminController::class ,'dashboared'])->name('admin.dashboared');
Route::prefix('category')->group(function () {

    Route::get('/', [CategoryController::class ,'index'])->name('admin.category');
    Route::get('/manage_category', [CategoryController::class ,'manage_category'])->name('admin.manage_category');
    Route::get('/manage_category/{id}', [CategoryController::class ,'manage_category'])->name('admin.manage_category.edit');
    Route::post('/manage_category_process', [CategoryController::class ,'manage_category_process'])->name('category.manage_category_process');
    Route::get('/delete/{id}', [CategoryController::class ,'delete'])->name('admin.delete');
    Route::get('/status/{status}/{id}', [CategoryController::class ,'status']);

});
Route::prefix('coupon')->group(function () {
    Route::get('/', [CouponController::class ,'index'])->name('admin.coupon');
    Route::get('/manage_coupon', [CouponController::class ,'manage_coupon'])->name('admin.manage_coupon');
    Route::get('/manage_coupon/{id}', [CouponController::class ,'manage_coupon'])->name('admin.manage_coupon.edit');
    Route::post('/manage_coupon_process', [CouponController::class ,'manage_coupon_process'])->name('admin.manage_coupon_process');
    Route::get('/delete/{id}', [CouponController::class ,'delete'])->name('coupon.delete');
    Route::get('/status/{status}/{id}', [CouponController::class ,'status']);

});

Route::prefix('color')->group(function () {
    Route::get('/', [ColorController::class ,'index'])->name('admin.color');
    Route::get('/manage_color', [ColorController::class ,'manage_color'])->name('admin.manage_color');
    Route::get('/manage_color/{id}', [ColorController::class ,'manage_color'])->name('admin.manage_color.edit');
    Route::post('/manage_color_process', [ColorController::class ,'manage_color_process'])->name('admin.manage_color_process');
    Route::get('/delete/{id}', [ColorController::class ,'delete'])->name('color.delete');
    Route::get('/status/{status}/{id}', [ColorController::class ,'status']);

});

Route::prefix('size')->group(function () {
    Route::get('/', [SizeController::class ,'index'])->name('admin.size');
    Route::get('/manage_size', [SizeController::class ,'manage_size'])->name('admin.manage_size');
    Route::get('/manage_size/{id}', [SizeController::class ,'manage_size'])->name('admin.manage_size.edit');
    Route::post('/manage_size_process', [SizeController::class ,'manage_size_process'])->name('admin.manage_size_process');
    Route::get('/delete/{id}', [SizeController::class ,'delete'])->name('size.delete');
    Route::get('/status/{status}/{id}', [SizeController::class ,'status']);

});
Route::prefix('product')->group(function () {
    Route::get('/', [ProductController::class ,'index'])->name('admin.product');
    Route::get('/manage_product', [ProductController::class ,'manage_product'])->name('admin.manage_product');
    Route::get('/manage_product/{id}', [ProductController::class ,'manage_product'])->name('admin.manage_product.edit');
    Route::post('/manage_product_process', [ProductController::class ,'manage_product_process'])->name('admin.manage_product_process');
    Route::get('/delete/{id}', [ProductController::class ,'delete'])->name('product.delete');
    Route::get('/status/{status}/{id}', [ProductController::class ,'status']);

});
    Route::get('/logout', [AdminController::class ,'logout'])->name('admin.logout');

    // Route::get('/updatepassword', [AdminController::class ,'updatepassword']);

});
});

