<aside class="menu-sidebar d-none d-lg-block">
    <div class="logo">
        <a href="#">
            <img src="{{ asset('admin_assets/images/icon/logo.png') }}" alt="Cool Admin" />
        </a>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">
                <li class="@yield('class_dashboared')">
                    <a href="{{ route('admin.dashboared') }}">
                        <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                </li>
                <li class="@yield('class_category')">
                    <a href="{{ route('admin.category') }}">
                        <i class="fas fa-list"></i>Category</a>
                </li>
                <li class="@yield('class_coupon')">
                    <a href="{{ route('admin.coupon') }}">
                        <i class="fas fa-tag"></i>Coupon</a>
                </li>
                <li class="@yield('size_selected')">
                    <a href="{{ route('admin.size') }}">
                        <i class="fas fa-tag"></i>Size</a>
                </li>
                <li class="@yield('color_selected')">
                    <a href="{{ route('admin.color') }}">
                        <i class="fas fa-paint-brush"></i>Color</a>
                </li>
                <li class="@yield('product_selected')">
                    <a href="{{ route('admin.product') }}">
                         <i class="fab fa-product-hunt"></i>Product</a>
                </li>

            </ul>
        </nav>
    </div>
</aside>
