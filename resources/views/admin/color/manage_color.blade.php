@extends('layouts.admin')
@section('page_title', 'Manage Color')
@section('color_selected', 'active')

@section('content')
    <h1>Color</h1><br>
    <a class="btn btn-primary" href="{{ route('admin.color') }}" role="button">Back</a>
    <div class="container">
        <div class="row m-t-30">
            <div class="col-10 offset-1 col-lg-8 offset-lg-2 div-wrapper d-flex justify-content-center align-items-center">
                <div class="card">
                    <div class="card-header">Manage Color</div>
                    <div class="card-body">

                        <form action="{{ route('admin.manage_color_process') }}" method="post" novalidate="novalidate">
                            @csrf
                            @error('color')
                                <div class="alert alert-danger" role="alert">
                                    {{ $message }}
                                </div>
                            @enderror


                        <div class="form-group has-success">
                            <label for="color" class="control-label mb-1">Color</label>
                            <input id="color" value="{{ $color }}" name="color" type="text"
                                class="form-control cc-name valid" autofocus required>
                        </div>
                        <input type="hidden" value="{{ $id }}}" name="id">
                        <div>
                            <button id="button" type="submit" class="btn btn-lg btn-info btn-block">
                                Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
