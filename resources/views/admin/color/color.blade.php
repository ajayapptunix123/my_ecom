@extends('layouts.admin')
@section('page_title', 'Color')
@section('color_selected', 'active')
@section('content')
    <h1>Color</h1><br>
    @if (session('success'))

        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @if (session('delete'))

        <div class="alert alert-danger" role="alert">
            {{ session('delete') }}
        </div>
    @endif
    <a class="btn btn-primary" href="{{ route('admin.manage_color') }}" role="button">Add color</a>
    <div class="row m-t-30">
        <div class="table-responsive m-b-40">
            <table class="table table-borderless table-data3">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Color</th>
                        <th>Actions</th>


                    </tr>
                </thead>
                <tbody>
                    @foreach ($model as $list)
                        <tr>

                            <td>{{ $list->id }}</td>
                            <td>{{ $list->color }}</td>

                            <td> <a class="btn btn-success"
                                    href="{{ route('admin.manage_color.edit', $list->id) }}">Edit</a>
                                @if ($list->status == 0)
                                    <a class="btn btn-warning"
                                        href="{{ url('admin/color/status/1') }}/{{ $list->id }}">Deactive</a>

                                @elseif($list->status==1)

                                    <a class="btn btn-primary"
                                        href="{{ url('admin/color/status/0') }}/{{ $list->id }}">Active</a>

                                @endif

                                <a class="btn btn-danger" onclick="return confirm('Are you sure?')"
                                    href="{{ route('color.delete', $list->id) }}">Delete</a>
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
