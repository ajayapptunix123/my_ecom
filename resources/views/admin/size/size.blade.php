@extends('layouts.admin')
@section('page_title', 'Size')
@section('size_selected', 'active')
@section('content')
    <h1>Size</h1><br>
    @if (session('success'))

        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @if (session('delete'))

        <div class="alert alert-danger" role="alert">
            {{ session('delete') }}
        </div>
    @endif
    <a class="btn btn-primary" href="{{ route('admin.manage_size') }}" role="button">Add size</a>
    <div class="row m-t-30">
        <div class="table-responsive m-b-40">
            <table class="table table-borderless table-data3">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Size</th>
                        <th>Actions</th>


                    </tr>
                </thead>
                <tbody>
                    @foreach ($model as $list)
                        <tr>

                            <td>{{ $list->id }}</td>
                            <td>{{ $list->size }}</td>

                            <td> <a class="btn btn-success"
                                    href="{{ route('admin.manage_size.edit', $list->id) }}">Edit</a>
                                @if ($list->status == 0)
                                    <a class="btn btn-warning"
                                        href="{{ url('admin/size/status/1') }}/{{ $list->id }}">Deactive</a>

                                @elseif($list->status==1)

                                    <a class="btn btn-primary"
                                        href="{{ url('admin/size/status/0') }}/{{ $list->id }}">Active</a>

                                @endif

                                <a class="btn btn-danger" onclick="return confirm('Are you sure to delete?')"
                                    href="{{ route('size.delete', $list->id) }}">Delete</a>
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
