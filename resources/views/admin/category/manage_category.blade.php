@extends('layouts.admin')
@section('page_title', 'Mangage Category')
@section('class_category', 'active')
@section('content')
    <h1>Category</h1><br>
    <a class="btn btn-primary" href="{{ route('admin.category') }}" role="button">Back</a>
    <div class="container">
        <div class="row m-t-30">
            <div class="col-10 offset-1 col-lg-8 offset-lg-2 div-wrapper d-flex justify-content-center align-items-center">
                <div class="card">
                    <div class="card-header">Manage Category</div>
                    <div class="card-body">

                        <form action="{{ route('category.manage_category_process') }}" method="post"
                            novalidate="novalidate">
                            @csrf
                            @error('category_name')
                                <div class="alert alert-danger" role="alert">
                                    {{ $message }}
                                </div>
                            @enderror
                            @error('category_slug')
                                <div class="alert alert-danger" role="alert">
                                    {{ $message }}
                                </div>
                            @enderror
                            <div class="form-group has-success">
                                <label for="category_name" class="control-label mb-1">Category Name</label>
                                <input id="category_name" value="{{ $category_name }}" name="category_name" type="text"
                                    class="form-control cc-name valid" autofocus required>
                            </div>


                            <div class="form-group has-success">
                                <label for="category_slug" class="control-label mb-1">Category Slug</label>
                                <input id="category_slug" value="{{ $category_slug }}" name="category_slug" type="text"
                                    class="form-control cc-name valid" required>
                            </div>
                            <input type="hidden" value="{{ $id }}}" name="id">
                            <div>
                                <button id="button" type="submit" class="btn btn-lg btn-info btn-block">
                                    Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
