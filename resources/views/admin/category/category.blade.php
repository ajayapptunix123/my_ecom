@extends('layouts.admin')
@section('page_title','Category')
@section('class_category', 'active')
@section('content')
    <h1>Category</h1><br>
    @if (session('success'))

    <div class="alert alert-success" role="alert">
       {{ session('success') }}
    </div>
    @endif
    @if (session('delete'))

    <div class="alert alert-danger" role="alert">
       {{ session('delete') }}
    </div>
    @endif
    <a class="btn btn-primary" href="{{ route('admin.manage_category') }}" role="button">Add Category</a>
 <div class="row m-t-30">
    <div class="table-responsive m-b-40">
        <table class="table table-borderless table-data3">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Category</th>
                    <th>Category Slug</th>
                    <th>Action</th>
                   </tr>
            </thead>
            <tbody>
                @foreach ($data as  $list)
                <tr>

                    <td>{{ $list->id }}</td>
                    <td>{{ $list->category_name }}</td>
                    <td>{{ $list->category_slug }}</td>
                    <td>
                    <a class="btn btn-success"  href="{{ route('admin.manage_category.edit', $list->id) }}">Edit</a>

                        @if ($list->status==1)
                        <a class="btn btn-primary"  href="{{  url('admin/category/status/0')  }}/{{ $list->id }}">Activate</a>

                        @elseif($list->status==0)
                        <a class="btn btn-warning"  href="{{ url('admin/category/status/1') }}/{{ $list->id }}">Deactivate</a>

                        @endif
                    <a class="btn btn-danger" onclick="return confirm('Are you sure to delete?')" href="{{ route('admin.delete', $list->id) }}">Delete</a>

                    </td>
                </tr>
 @endforeach
            </tbody>
        </table>
    </div>
 </div>
@endsection
