@extends('layouts.admin')
@section('page_title', 'Coupon')
@section('class_coupon', 'active')
@section('content')
    <h1>Coupon</h1><br>
    @if (session('success'))

        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @if (session('delete'))

        <div class="alert alert-danger" role="alert">
            {{ session('delete') }}
        </div>
    @endif
    <a class="btn btn-primary" href="{{ route('admin.manage_coupon') }}" role="button">Add coupon</a>
    <div class="row m-t-30">
        <div class="table-responsive m-b-40">
            <table class="table table-borderless table-data3">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Code</th>
                        <th>Value</th>
                        <th style= text-align:center>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $list)
                        <tr>

                            <td>{{ $list->id }}</td>
                            <td>{{ $list->title }}</td>
                            <td>{{ $list->code }}</td>
                            <td>{{ $list->value }}</td>
                            <td> <a class="btn btn-success"
                                    href="{{ route('admin.manage_coupon.edit', $list->id) }}">Edit</a>
                                @if ($list->status == 0)
                                    <a class="btn btn-warning"
                                        href="{{ url('admin/coupon/status/1') }}/{{ $list->id }}">Deactive</a>

                                @elseif($list->status==1)

                                    <a class="btn btn-primary"
                                        href="{{ url('admin/coupon/status/0') }}/{{ $list->id }}">Active</a>

                                @endif

                                <a class="btn btn-danger" onclick="return confirm('Are you sure?')"
                                    href="{{ route('coupon.delete', $list->id) }}">Delete</a>
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
