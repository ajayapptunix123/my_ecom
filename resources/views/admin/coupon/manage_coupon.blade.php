@extends('layouts.admin')
@section('page_title', 'Manage Coupon')
@section('class_coupon', 'active')

@section('content')
    <h1>Coupon</h1><br>
    <a class="btn btn-primary" href="{{ route('admin.coupon') }}" role="button">Back</a>
    <div class="container">
        <div class="row m-t-30">
            <div class="col-10 offset-1 col-lg-8 offset-lg-2 div-wrapper d-flex justify-content-center align-items-center">
                <div class="card">
                    <div class="card-header">Manage Coupon</div>
                    <div class="card-body">

                        <form action="{{ route('admin.manage_coupon_process') }}" method="post"
                            novalidate="novalidate">
                            @csrf
                            @error('title')
                                <div class="alert alert-danger" role="alert">
                                    {{ $message }}
                                </div>
                            @enderror
                            @error('code')
                                <div class="alert alert-danger" role="alert">
                                    {{ $message }}
                                </div>
                            @enderror
                            @error('value')
                            <div class="alert alert-danger" role="alert">
                                {{ $message }}
                            </div>
                        @enderror
                            <div class="form-group has-success">
                                <label for="title" class="control-label mb-1">Title</label>
                                <input id="title" value="{{ $title }}" name="title" type="text"
                                    class="form-control cc-name valid" autofocus required>
                            </div>


                            <div class="form-group has-success">
                                <label for="code" class="control-label mb-1">Code</label>
                                <input id="code" value="{{ $code }}" name="code" type="text"
                                    class="form-control cc-name valid" required>
                            </div>
                            <div class="form-group has-success">
                                <label for="value" class="control-label mb-1">Value</label>
                                <input id="value" value="{{ $value }}" name="value" type="text"
                                    class="form-control cc-name valid" required>
                            </div>
                            <input type="hidden" value="{{ $id }}}" name="id">
                            <div>
                                <button id="button" type="submit" class="btn btn-lg btn-info btn-block">
                                    Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
