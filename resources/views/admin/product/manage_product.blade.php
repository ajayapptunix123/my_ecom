@extends('layouts.admin')
@section('page_title', 'Manage Product')
@section('product_selected', 'active')
@section('content')

    <h1>Product</h1><br>
    <a class="btn btn-primary" href="{{ route('admin.product') }}" role="button">Back</a>
    <div class="container">
        <div class="row m-t-30">
    {{-- <div class="table-responsive m-b-40"> --}}
        <form action="{{ route('admin.manage_product_process') }}" method="post" novalidate="novalidate" enctype="multipart/form-data">

                <div class="card">
                    {{-- <div class="card-header">Manage Product</div> --}}
                    <div class="card-body">

                            @csrf
                            @error('name')
                                <div class="alert alert-danger" role="alert">
                                    {{ $message }}
                                </div>
                            @enderror
                            @error('slug')
                                <div class="alert alert-danger" role="alert">
                                    {{ $message }}
                                </div>
                            @enderror
                            @error('image')
                                <div class="alert alert-danger" role="alert">
                                    {{ $message }}
                                </div>
                            @enderror
                            <div class="form-group has-success">
                                <label for="name" class="control-label mb-1"> Name</label>
                                <input id="name" value="{{ $name }}" name="name" type="text"
                                    class="form-control cc-name valid" autofocus required>
                            </div>


                            <div class="form-group has-success">
                                <label for="slug" class="control-label mb-1">Slug</label>
                                <input id="slug" value="{{ $slug }}" name="slug" type="text"
                                    class="form-control cc-name valid" required>
                            </div>
                            <div class="form-group has-success">
                                <label for="image" class="control-label mb-1">Image</label>
                                <input id="image" type="file" value="{{ $image }}" name="image" type="text"
                                    class="form-control cc-name valid" >
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                <label for="category_id" class="control-label mb-1">Category</label>
                                <select name="category_id" id="category_id" type="text" class="form-control cc-name valid">
                                    <option value="">Select Category</option>
                                    @foreach ($category as $list)
                                        @if ($category_id == $list->id)
                                            <option selected value="{{ $list->id }}">
                                            @else
                                            <option value="{{ $list->id }}">
                                        @endif


                                        {{ $list->category_name }}</option>

                                    @endforeach
                                </select>
                                </div>
                                <div class="col-md-4">


                                <label for="brand" class="control-label mb-1">Brand</label>
                                <input id="brand" value="{{ $brand }}" name="brand" type="text"
                                    class="form-control cc-name valid" required>
                                </div>

                                <div class="col-md-4">
                                <label for="model" class="control-label mb-1">Model</label>
                                <input id="model" value="{{ $model }}" name="model" type="text"
                                    class="form-control cc-name valid" required>
                            </div>
                        </div>
                            <div class="form-group has-success">
                                <label for="short_desc" class="control-label mb-1">Short description</label>
                                <textarea id="short_desc" name="short_desc" type="text" class="form-control cc-name valid"
                                    required>{{ $short_desc }}</textarea>

                            </div>
                            <div class="form-group has-success">
                                <label for="desc" class="control-label mb-1">Description</label>
                                <textarea id="desc" name="desc" type="text" class="form-control cc-name valid"
                                    required>{{ $desc }}</textarea>

                            </div>
                            <div class="form-group has-success">
                                <label for="keywords" class="control-label mb-1">keywords</label>
                                <textarea id="keywords" name="keywords" type="text" class="form-control cc-name valid"
                                    required>{{ $keywords }}</textarea>

                            </div>
                            <div class="form-group has-success">
                                <label for="technical_specification" class="control-label mb-1">Technical
                                    Specification</label>
                                <textarea id="technical_specification" name="technical_specification" type="text"
                                    class="form-control cc-name valid" required>{{ $technical_specification }}</textarea>

                            </div>
                            <div class="form-group has-success">
                                <label for="uses" class="control-label mb-1">Uses</label>
                                <textarea id="uses" name="uses" type="text" class="form-control cc-name valid"
                                    required>{{ $uses }}</textarea>

                            </div>
                            <div class="form-group has-success">
                                <label for="warranty" class="control-label mb-1">Warranty</label>
                                <textarea id="warranty" name="warranty" type="text" class="form-control cc-name valid"
                                    required>{{ $warranty }}</textarea>

                            </div>
                            <input type="hidden" value="{{ $id }}}" name="id">


                    </div>
                </div>


                        <h2 > Product Attribute </h2>

<div id="product_attr_box"></div>

                <div class="card" id="product_attr_box"><br>

                        <div class="card-body" id="product_attr_1">
                            <div class="row">

                                <div class="col-md-2">


                                <label for="brand" class="control-label mb-1">SKU</label>
                                <input id="sku" name="sku" type="text"
                                    class="form-control cc-name valid" required>
                                </div>

                                <div class="col-md-2">
                                    <label for="mrp" class="control-label mb-1">MRP</label>
                                    <input id="mrp" name="mrp" type="text"
                                        class="form-control cc-name valid" required>
                                </div>
                                <div class="col-md-2">
                                    <label for="price" class="control-label mb-1">PRICE</label>
                                    <input id="price" name="price" type="text"
                                        class="form-control cc-name valid" required>
                                </div>
                                <div class="col-md-2">
                                    <label for="size_id" class="control-label mb-1">Size</label>
                                    <select name="size_id" id="size_id" type="text" class="form-control cc-name valid">
                                        <option value="">Select Size</option>
                                        @foreach ($size as $list)

                                                <option value="{{ $list->id }}">



                                            {{ $list->size }}</option>

                                        @endforeach
                                    </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="color_id" class="control-label mb-1">Color</label>
                                        <select name="color_id" id="color_id" type="text" class="form-control cc-name valid">
                                            <option value="">Select Color</option>
                                            @foreach ($color as $list)

                                                    <option value="{{ $list->id }}">



                                                {{ $list->color }}</option>

                                            @endforeach
                                        </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label for="qty" class="control-label mb-1">Quantity</label>
                                            <input id="qty" name="qty" type="text"
                                                class="form-control cc-name valid" required>
                                        </div>
                        </div>
                        <div class="row">

                            <div class="col-md-4">

                                <label for="attr_image" class="control-label mb-1">Image</label>
                                <input id="attr_image" type="file" name="attr_image" type="text"
                                    class="form-control cc-name valid" >
                            </div>
                            <div class="col-md-2 mlg">

                                <button type="button" id="button" class="btn btn-success btn-lg " onclick="add_more()">
                                    <i class="fa fa-magic"></i>&nbsp; Add</button>
                            </div>
                        </div>
                    </div>
                </div>

        </div>
        <button id="button" type="submit" class="btn btn-lg btn-info btn-block">
            Submit
        </button>
    </form>
    </div>




<script>
var loop_count=1;
    var add_more = function() {

        loop_count++;
    var html ='<div class="card " id="product_attr_'+loop_count+'"><div class="card-body"><div class="row">';
    html+=' <div class="col-md-2"><label for="sku" class="control-label mb-1">SKU</label><input id="sku" name="sku" type="text"class="form-control cc-name valid" required></div>';
    html+=' <div class="col-md-2"><label for="mrp" class="control-label mb-1">MRP</label><input id="mrp" name="mrp" type="text"class="form-control cc-name valid" required></div>';
    html+=' <div class="col-md-2"><label for="price" class="control-label mb-1">Price</label><input id="price" name="price" type="text"class="form-control cc-name valid" required></div>';
    var size_id_html =jQuery('#size_id').html();
    html+='<div class="col-md-2"><label for="size_id" class="control-label mb-1">Size</label><select name="size_id" id="size_id" type="text" class="form-control cc-name valid">'+size_id_html+'</select></div> ';
    var color_id_html =jQuery('#color_id').html();
    html+='<div class="col-md-2"><label for="color_id" class="control-label mb-1">Color</label><select name="color_id" id="color_id" type="text" class="form-control cc-name valid">'+color_id_html+'</select></div> ';
    html+='<div class="col-md-2"><label for="qty" class="control-label mb-1">Quantity</label><input id="qty" name="qty" type="text"class="form-control cc-name valid" required></div>';
    html+='<div class="col-md-4"><label for="attr_image" class="control-label mb-1">Image</label><input id="attr_image" name="attr_image" type="file"class="form-control cc-name valid" required></div>';
    html+='<div class="col-md-2 mlg"><button type="button"  class="btn btn-danger btn-sm " onclick="remove_more('+loop_count+')"><i class="fa fa-minus"></i>&nbsp; Remove</button></div>';


    $('#product_attr_box').append(html);
    }
     function remove_more(loop_count) {
        debugger;
        jQuery("#product_attr_"+loop_count).remove();

    }
    </script>
@endsection
