@extends('layouts.admin')
@section('page_title','Product')
@section('product_selected', 'active')
@section('content')
    <h1>Product</h1><br>
    @if (session('success'))

    <div class="alert alert-success" role="alert">
       {{ session('success') }}
    </div>
    @endif
    @if (session('delete'))

    <div class="alert alert-danger" role="alert">
       {{ session('delete') }}
    </div>
    @endif
    <a class="btn btn-primary" href="{{ route('admin.manage_product') }}" role="button">Add product</a>
 <div class="row m-t-30">
    <div class="table-responsive m-b-40">
        <table class="table table-borderless table-data3">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Slug</th>
                    <th>Image</th>
                    <th>Action</th>
                   </tr>
            </thead>
            <tbody>
                @foreach ($data as  $list)
                <tr>

                    <td>{{ $list->id }}</td>
                    <td>{{ $list->name }}</td>
                    <td>{{ $list->slug }}</td>
                    <th><img src="{{ $list->profile_photo_url }}" width="50px" alt="img"></th>

                    <td>
                    <a class="btn btn-success"  href="{{ route('admin.manage_product.edit', $list->id) }}">Edit</a>

                        @if ($list->status==1)
                        <a class="btn btn-primary"  href="{{  url('admin/product/status/0')  }}/{{ $list->id }}">Active</a>

                        @elseif($list->status==0)
                        <a class="btn btn-warning"  href="{{ url('admin/product/status/1') }}/{{ $list->id }}">Deactive</a>

                        @endif
                    <a class="btn btn-danger" onclick="return confirm('Are you sure to delete?')" href="{{ route('admin.delete', $list->id) }}">Delete</a>

                    </td>
                </tr>
 @endforeach
            </tbody>
        </table>
    </div>
 </div>
@endsection
