<?php

namespace App\Http\Controllers;

use App\Models\Size;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model=Size::all();
        return view('admin.size.size' ,compact('model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage_size(Request $request, $id="")
    {
        if ($id>0) {
           $arr=Size::where(['id'=>$id])->get();
           $result['size']=$arr['0']->size;
           $result['id']=$arr['0']->id;
        }
        else{
            $result['size']="";
            $result['id']="0";
        }
        return view('admin.size.manage_size' ,$result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function manage_size_process(Request $request)
    {
        $request->validate([
            'size'=>'required|unique:sizes,size,'.$request->post('id')
        ]);
        if ($request->post('id')>0) {
           $model=Size::find($request->post('id'));
           $msg="Size updated successfully";
        }
        else{
$model= new Size();
$msg="Size updated successfully";

        }
        $model->size=$request->size;
        $model->status=1;
        $model->save();
        $request->session()->flash('success',$msg);
        return redirect()->route('admin.size');
    }



    public function delete(Request $request, $id)
    {
        $model=Size::find($id);
        $model->delete();
        $request->session()->flash('delete','size Deleted Successfully');
        return redirect()->route('admin.size');
    }
    public function status(Request $request,$status, $id)
    {
        $model=Size::find($id);
        $model->status=$status;
        $model->save();
        $request->session()->flash('success','Status updated Successfully');
        return redirect()->route('admin.size');
    }
}
