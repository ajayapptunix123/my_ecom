<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if( $request->session()->has('ADMIN_LOGIN')){
            return view('admin.dashboared');
        }
        else
        {

          return view('admin.login');
        }

    }




    public function auth(Request $request)
    {
        $email = $request->post('email');
        $password = $request->post('password');

        // $result = Admin::where(['email'=>$email ,'password'=>$password])->get();
        $result = Admin::where(['email'=>$email])->first();
       if($result){
           if(Hash::check($password, $result->password)){
            $request->session()->put('ADMIN_LOGIN', true);

            $request->session()->put('ADMIN_ID', $result->id);

            return redirect('admin/dashboared');
           }
           else{
            $request->session()->flash('error','Please enter correct password');
            return redirect('admin');
           }

       }
       else{
         $request->session()->flash('error','Please enter valid caredentials');
         return redirect('admin');
       }
    }
       public function dashboared()
       {
           return view('admin.dashboared');
       }
    //    public function updatepassword()
    //    {
    //        $r =Admin::find(1);
    //        $r->password =Hash::make(123);
    //        $r->save();

    //    }

    public function logout(Request $request)
    {
        $request->session()->invalidate();


        $request->session()->flash('error','Logout successfully');
        return redirect('admin');
    }
}






