<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
class CategoryController extends Controller
{

    public function index()
    {
           $data=Category::all();

       return  view('admin.category.category', compact('data'));

    }


    public function manage_category(Request $request ,$id="")
    {
        if ($id>0) {
            $arr=Category::where(['id'=>$id])->get();
            $result['category_name']=$arr['0']->category_name;
            $result['category_slug']=$arr['0']->category_slug;
            $result['id']=$arr['0']->id;

        }else{
            $result['category_name']="";
            $result['category_slug']="";
            $result['id']="0";

        }

        return view('admin.category.manage_category',$result);
    }

    public function manage_category_process(Request $request)
    {
        $request->validate([
            'category_name'=> 'required',
            'category_slug'=>'required|unique:categories,category_slug,'.$request->post('id'),
        ]);

        // $model = new Category();


        if ($request->post('id')>0) {
           $model=Category::find($request->post('id'));
           $msg='Category updated successfully';
        }else{
             $model = new Category();
           $msg='Category added successfully';

        }
        // $model->category_name=$title=$request->category_name;
        //  $model->category_slug=Str::slug($title). '-'. Str::random(); /*first letter to small*/
        //  $model->category_slug=Str::title($title);/*first letter to capital*/

         $model->category_name=$request->category_name;
         $model->category_slug=$request->category_slug;
         $model->status=1;
        $model ->save();

        $request->session()->flash('success',$msg);
        return redirect()->route('admin.category');
    }

    public function delete(Request $request, $id)
    {
        $data=Category::find($id);
        $data->delete();
        return redirect()->route('admin.category')->with('delete', 'Category Deleted successfully');

    }
    public function status(Request $request,$status,$id)
    {
        $data=Category::find($id);
        $data->status=$status;

        $data->save();
        return redirect()->route('admin.category')->with('successs', 'Status updated successfully');


    }
}
