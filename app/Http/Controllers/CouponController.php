<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           $data=Coupon::all();

           return   view('admin.coupon.coupon', compact('data'));

    }


    public function manage_coupon(Request $request ,$id="")
    {

        if ($id>0) {
            $arr=Coupon::where(['id'=>$id])->get();
            $result['title']=$arr['0']->title;
            $result['code']=$arr['0']->code;
            $result['value']=$arr['0']->value;
            $result['id']=$arr['0']->id;

        }else{
            $result['title']="";
            $result['code']="";
            $result['value']="";
            $result['id']="0";

        }


        return view('admin.coupon.manage_coupon',$result);
    }

    public function manage_coupon_process(Request $request)
    {

        $request->validate([
            'title'=> 'required',
            'value'=> 'required',
            'code'=>'required|unique:coupons,code,'.$request->post('id'),
        ]);

        // $model = new coupon();


        if ($request->post('id')>0) {
           $model=Coupon::find($request->post('id'));
           $msg=' Coupon updated successfully';
        }else{
             $model = new Coupon();
           $msg='Coupon added successfully';

        }

         $model->title=$request->title;
         $model->code=$request->code;
         $model->value=$request->value;
         $model->status=1;
         $model ->save();

        $request->session()->flash('success',$msg);
        return redirect()->route('admin.coupon');
    }

    public function delete(Request $request, $id)
    {
        $data=Coupon::find($id);
        $data->delete();
        return redirect()->route('admin.coupon')->with('delete', 'Coupon deleted successfully');
    }
        public function status(Request $request,$status, $id)
        {
            $data=Coupon::find($id);

            $data->status=$status;
            $data->save();
            return redirect()->route('admin.coupon')->with('success', 'Status updated successfully');

        }


}
