<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $data=Product::all();
        return view('admin.product.product', compact('data'));

    }


    public function manage_product(Request $request ,$id="")
    {

        if ($id>0) {
            $arr=Product::where(['id'=>$id])->get();
            $result['category_id']=$arr['0']->category_id;
            $result['name']=$arr['0']->name;
            $result['image']=$arr['0']->image;
            $result['slug']=$arr['0']->slug;
            $result['brand']=$arr['0']->brand;
            $result['model']=$arr['0']->model;
            $result['short_desc']=$arr['0']->short_desc;
            $result['desc']=$arr['0']->desc;
            $result['keywords']=$arr['0']->keywords;
            $result['technical_specification']=$arr['0']->technical_specification;
            $result['uses']=$arr['0']->uses;
            $result['warranty']=$arr['0']->warranty;
            $result['status']=$arr['0']->status;
            $result['id']=$arr['0']->id;

        }else{
            $result['category_id']="";
            $result['name']="";
            $result['image']="";
            $result['slug']="";
            $result['brand']="";
            $result['model']="";
            $result['short_desc']="";
            $result['desc']="";
            $result['keywords']="";
            $result['technical_specification']="";
            $result['uses']="";
            $result['warranty']="";
            $result['status']="";
            $result['id']="0";


        }

        $result['category']=DB::table('categories')->where(['status'=>1])->get();
        $result['size']=DB::table('sizes')->where(['status'=>1])->get();
        $result['color']=DB::table('colors')->where(['status'=>1])->get();

        return view('admin.product.manage_product',$result);
    }

    public function manage_product_process(Request $request)
    {

        if ($request->post('id')>0) {
            $image_selected="mimes:png,jpg,jpeg";
         }else{
            $image_selected="required|mimes:png,jpg,jpeg";


         }
        $request->validate([
            'name'=> 'required',
            'image'=> $image_selected,
            'slug'=>'required|unique:products,slug,'.$request->post('id'),
        ]);




        if ($request->post('id')>0) {
           $model=Product::find($request->post('id'));
           $msg='Product updated successfully';
        }else{
             $model = new product();
           $msg='Product added successfully';

        }

        if ($request->hasFile('image')){
            $model['image'] = $this->fileUpload($request->image, 'assets/images')['name'] ?? null;
        }


         $model->name=$request->name;
         $model->slug=$request->slug;
        //  $model->image=$request->image;
         $model->category_id=$request->category_id;
         $model->brand=$request->brand;
         $model->model=$request->model;
         $model->short_desc=$request->short_desc;
         $model->desc=$request->desc;
         $model->keywords=$request->keywords;
         $model->technical_specification=$request->technical_specification;
         $model->uses=$request->uses;
         $model->warranty=$request->warranty;
         $model->status=1;

        $model ->save();

        $request->session()->flash('success',$msg);
        return redirect()->route('admin.product');
    }

    public function delete(Request $request, $id)
    {
        $data=product::find($id);
        $data->delete();
        return redirect()->route('admin.product')->with('delete', 'Product Deleted successfully');

    }
    public function status(Request $request,$status,$id)
    {
        $data=Product::find($id);
        $data->status=$status;

        $data->save();
        return redirect()->route('admin.product')->with('successs', 'Status updated successfully');


    }
}
