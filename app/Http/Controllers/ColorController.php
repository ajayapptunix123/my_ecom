<?php

namespace App\Http\Controllers;

use App\Models\Color;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model=Color::all();
        return view('admin.color.color' ,compact('model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage_color(Request $request, $id="")
    {
        if ($id>0) {
           $arr=Color::where(['id'=>$id])->get();
           $result['color']=$arr['0']->color;
           $result['id']=$arr['0']->id;
        }
        else{
            $result['color']="";
            $result['id']="0";
        }
        return view('admin.color.manage_color' ,$result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function manage_color_process(Request $request)
    {
        $request->validate([
            'color'=>'required|unique:colors,color,'.$request->post('id')
        ]);
        if ($request->post('id')>0) {
           $model=Color::find($request->post('id'));
           $msg="Color updated successfully";
        }
        else{
$model= new Color();
$msg="Color updated successfully";

        }
        $model->color=$request->color;
        $model->status=1;
        $model->save();
        $request->session()->flash('success',$msg);
        return redirect()->route('admin.color');
    }



    public function delete(Request $request, $id)
    {
        $model=Color::find($id);
        $model->delete();
        $request->session()->flash('delete','Color Deleted Successfully');
        return redirect()->route('admin.color');
    }
    public function status(Request $request,$status, $id)
    {
        $model=Color::find($id);
        $model->status=$status;
        $model->save();
        $request->session()->flash('success','Status updated Successfully');
        return redirect()->route('admin.color');
    }
}
