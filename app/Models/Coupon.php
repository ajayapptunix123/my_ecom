<?php

namespace App\Models;
// use App\Casts\MoneyCast;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;

    // public function setValueAttribute($value)
    // {
    //     $this->attributes['value'] = $value * 70;
    // }
    protected $casts = [
        'status' => 'boolean',
        // 'value' => MoneyCast::class,
    ];
}
