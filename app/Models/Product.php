<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $appends = [
        'profile_photo_url',

    ];

public function getProfilePhotoUrlAttribute()
    {
        if (preg_match('(https://|http://)', $this->image) === 1) {
            return $this->image;
        }
        return !empty($this->image) ? asset("storage/assets/images/$this->image") : asset('storage/assets/images/user.png');
    }
    protected $casts = [
        'status' => 'boolean',


    ];
}
