<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    use HasFactory;

    public function getColorAttribute($value)
    {
        return ucfirst($value);
    }
    protected $casts = [
        'status' => 'boolean',
    ];

}
